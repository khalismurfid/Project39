
public class Manusia{
	
	private float kebahagiaan =50;
	private String nama;
	private int umur;
	private int uang = 50000;
	private boolean hidup = true;
	static Manusia temp;

	
	
	// Inititialize
	
	public Manusia(String nama, int umur ){
		this.nama = nama;
		this.umur = umur;
		temp = this;
	}
	
	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		temp = this;
	}
	public Manusia(int nama, String umur ){
		temp = this;
	}
	
	
	// Setter Getter
	
	public void setNama(String nama){
		this.nama = nama;
	}
	
	public void setUmur(int umur){
		this.umur = umur;
	}
	
	public void setUang(int uang){
		this.uang = uang;
	}
	
	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan > 100){
			this.kebahagiaan = 100;
		}
		else if (kebahagiaan < 0 ){
			this.kebahagiaan = 0;
		}
		else{
			this.kebahagiaan = kebahagiaan;
		}
	}
	
	public void setHidup(boolean hidup){
		this.hidup = hidup;
	}
	
	public String getNama(){
		return nama;
	}
	
	public int getUmur(){
		return umur;
	}
	
	public int getUang(){
		return uang;
	}
	
	public float getKebahagiaan(){
		return kebahagiaan;
	}
	
	public boolean getHidup(){
		return hidup;
	}
	
	
	// Method
	
	public void beriUang(Manusia penerima){
		if(getHidup() && penerima.getHidup()){
			int jumlah = 0;
			for(int a=0; a<penerima.getNama().length(); a++){
				int ascii = (int) penerima.getNama().charAt(a);
				jumlah+=ascii;
			}
			int jumlahuang = jumlah*100;
			if (getUang() >= jumlahuang){
				penerima.setUang(penerima.getUang()+jumlahuang);
				setUang(getUang()-jumlahuang);
				setKebahagiaan(getKebahagiaan()+ (float) jumlahuang/6000);
				penerima.kebahagiaan += (float) jumlahuang/6000;
				System.out.println(getNama()+ " memberi uang sebanyak "+Integer.toString(jumlahuang) +" kepada "+penerima.getNama()+", mereka berdua senang :D");	
		
			}
			else{
				System.out.println(getNama()+ " ingin memberi uang kepada " +penerima.getNama()+" namun tidak memiliki cukup uang :'(");
			}
		}
		else{
			System.out.println(getNama()+" telah tiada");
		}
		
	}
	
	public void beriUang(Manusia penerima, int jumlah){
		if(getHidup() && penerima.getHidup()){
			if (getUang() >= jumlah){
				penerima.setUang(penerima.getUang()+jumlah);
				setUang(getUang()-jumlah);
				setKebahagiaan(getKebahagiaan()+ (float) jumlah/6000);
				penerima.setKebahagiaan(penerima.getKebahagiaan() + (float) jumlah/6000);
				System.out.println(getNama()+ " memberi uang sebanyak "+Integer.toString(jumlah) +" kepada "+penerima.getNama()+ ", mereka berdua senang :D");
				
			}
			else{
				System.out.println(getNama()+ " ingin memberi uang kepada " +penerima.getNama()+" namun tidak memiliki cukup uang :'(");
			}
		}
		else{
			System.out.println("Orang mati hanya menyisakan amal sehingga tidak bisa memberi ataupun diberi uang");
		}
	}
	
	public void bekerja(int durasi, int bebankerja){
		if(getHidup()){
			int pendapatan;
			if (getUmur() <18){
				System.out.println(getNama()+" belum boleh bekerja karena masih dibawah umur D:");
			}
			else{
				int bebankerjatotal = durasi*bebankerja;
				if(bebankerjatotal<=getKebahagiaan()){
					setKebahagiaan(getKebahagiaan()-(float)bebankerjatotal);
					pendapatan = bebankerjatotal*10000;
					System.out.println(getNama()+" bekerja full time, total pendapatan : "+pendapatan);
					setUang(getUang()+pendapatan);
				}
				else{
					int DurasiBaru = (int)getKebahagiaan()/bebankerja;
					bebankerjatotal = DurasiBaru * bebankerja;
					pendapatan = bebankerjatotal*10000;
					setKebahagiaan(getKebahagiaan()-(float) DurasiBaru*bebankerja);
					setUang(getUang()+pendapatan);
					System.out.println(getNama()+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+pendapatan);
					}
				}
			}
		else{
			System.out.println(getNama()+" telah tiada");
		}
	}

	
	public void rekreasi(String namaTempat){
		if(getHidup()){
			int biaya = namaTempat.length()*10000;
			if(getUang()>=biaya){
				setUang(getUang()-biaya);
				setKebahagiaan(getKebahagiaan()+namaTempat.length());
				System.out.println(getNama()+" berekreasi di "+namaTempat+", "+getNama()+" senang :)");
			}
			else{
				System.out.println(getNama()+" tidak mempunyai cukup uang untuk berekreasi di "+namaTempat+" :{");
			}
		}
		else{
			System.out.println(getNama()+" telah tiada");
		}
	}
	
	public void sakit(String namaPenyakit){
		if(getHidup()){
			setKebahagiaan(getKebahagiaan()-namaPenyakit.length());
			System.out.println(getNama()+" terkena penyakit "+ namaPenyakit+" :o");
		}
		else{
			System.out.println(getNama()+" telah tiada");
		}
		
	}
	
	
	public void meninggal(){
		if(getHidup()){
			if(temp!=this){
				setHidup(false);
				System.out.println(this.nama+" meninggal dengan tenang, kebahagiaan : "+getKebahagiaan());
				temp.setUang(temp.getUang()+getUang());
				setUang(getUang()-getUang());
				System.out.println("Semua harta "+getNama()+" disumbangkan untuk "+ temp.getNama());
			}
			else{
				setHidup(false);
				setUang(0);
				System.out.println(this.nama+" meninggal dengan tenang, kebahagiaan : "+getKebahagiaan());
				System.out.println("Semua harta "+getNama()+" hangus");
				
			}
		}
		else{
			System.out.println(getNama()+" telah tiada");
		}
	}
	
	public String toString(){
		if(getHidup()){
			return ("Nama\t\t: "+getNama()+"\nUmur\t\t: "+getUmur()+"\nUang\t\t: "+getUang()+"\nKebahagiaan\t: "+getKebahagiaan());
		}
		else{
			return ("Nama\t\t: Almarhum "+getNama()+"\nUmur\t\t: "+getUmur()+"\nUang\t\t: "+getUang()+"\nKebahagiaan\t: "+getKebahagiaan());
		}
	}
	
	
	
	
	
	
}