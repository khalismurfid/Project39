package xoxo;


import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;


    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {

      //Create and set up the window.
      JFrame frame = new JFrame("LABTERAKHIR");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      //Set up the content pane.
      Container pane = frame.getContentPane();

      pane.setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();

      messageField = new JTextField("Insert Message");
      c.weightx = 0.5;
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 2;    //2 columns wide
      c.ipady = 400;      //make this component tall
      pane.add(messageField, c);

      logField = new JTextArea();
      c.weightx = 0.5;
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 2;
      c.gridy = 0;
      c.gridwidth = 2;    //2 columns wide
      c.ipady = 400;      //make this component tall
      pane.add(logField, c);

      keyField = new JTextField("Insert Key");
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 0.5;
      c.gridx = 0;
      c.gridy = 1;
      c.ipady = 0;        //reset
      c.gridwidth = 1;    //reset
      pane.add(keyField, c);

      encryptButton = new JButton("Encrypt");
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 0.5;
      c.gridx = 1;
      c.gridy = 1;
      pane.add(encryptButton, c);

      decryptButton = new JButton("Decrypt");
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 0.5;
      c.gridx = 2;
      c.gridy = 1;
      pane.add(decryptButton, c);

      seedField = new JTextField("DEFAULT_SEED");
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 0.5;
      c.gridx = 3;
      c.gridy = 1;
      pane.add(seedField, c);

      //Display the window.
      frame.pack();
      frame.setVisible(true);
  }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     *
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}
