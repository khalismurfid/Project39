package xoxo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.*;
import xoxo.key.HugKey;
import xoxo.util.XoxoMessage;
import java.io.*;
/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;

    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
      ActionListener encryptionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String messageText = gui.getMessageText();
                String keyText = gui.getKeyText();
                String seedText = gui.getSeedText();
                int seed = HugKey.DEFAULT_SEED;
                if (!(seedText.equals("DEFAULT_SEED"))) {
                    seed = Integer.parseInt(seedText);
                }
                XoxoEncryption encryptionAlgorithm = null;
                try {
                    encryptionAlgorithm = new XoxoEncryption(keyText);
                } catch (KeyTooLongException er) {
                    JOptionPane.showMessageDialog(null, er.getMessage());
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, er.getMessage());
                }

                XoxoMessage xoxoMessage;
                String encryptedMessage;
                HugKey hugKey;

                if (messageText == null) {
                    JOptionPane.showMessageDialog(null, "There is no message to encrypt");
                } else {
                    try {
                        xoxoMessage = encryptionAlgorithm.encrypt(messageText, seed);
                        encryptedMessage = xoxoMessage.getEncryptedMessage();
                        hugKey = xoxoMessage.getHugKey();
                        File file = new File("outputencrypt.enc");
                        file.createNewFile();
                        FileWriter writer = new FileWriter(file);
                        writer.write(encryptedMessage);
                        writer.flush();
                        writer.close();
                        gui.appendLog(
                                "Encryption process succeed\n" +
                                        "Encrypted message :\n" +
                                        encryptedMessage + "\n" +
                                        "Hug Key :\n" +
                                        hugKey
                        );
                    } catch (RangeExceededException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    } catch (SizeTooBigException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    } catch (InvalidCharacterException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    } catch (IOException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    }
                }
            }
        };

        ActionListener decryptionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String messageText = gui.getMessageText();
                String keyText = gui.getKeyText();
                String seedText = gui.getSeedText();
                int seed = HugKey.DEFAULT_SEED;
                if (!(seedText.equals("DEFAULT_SEED"))) {
                    seed = Integer.parseInt(seedText);
                }

                //Decryption algorithm instantiation
                XoxoDecryption decryptionAlgorithm = null;
                try {
                    decryptionAlgorithm = new XoxoDecryption(keyText);
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, er.getMessage());
                }

                //Decrypt the message
                String decryptedMessage;

                if (messageText == null) {
                    JOptionPane.showMessageDialog(null, "There is no message to decrypt");
                } else {
                    try {
                        decryptedMessage = decryptionAlgorithm.decrypt(messageText, seed);
                        File file = new File("outputdecrypt.txt");
                        file.createNewFile();
                        FileWriter writer = new FileWriter(file);
                        writer.write(decryptedMessage);
                        writer.flush();
                        writer.close();
                        gui.appendLog(
                                "Decryption process succeed\n" +
                                        "Decrypted message :\n" +
                                        decryptedMessage + "\n"
                        );
                    } catch (RangeExceededException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    } catch (SizeTooBigException er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    } catch (Exception er) {
                        JOptionPane.showMessageDialog(null, er.getMessage());
                    }
                }
            }
        };

        gui.setEncryptFunction(encryptionListener);

        gui.setDecryptFunction(decryptionListener);
    }

    //TODO: Create any methods that you want
}
