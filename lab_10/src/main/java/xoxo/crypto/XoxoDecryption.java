package xoxo.crypto;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.util.XoxoMessage;
/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
      if (seed <HugKey.MIN_RANGE || seed>HugKey.MAX_RANGE) {
            throw new RangeExceededException("Seed exceeds 0-36 range!");
        }
      if ((encryptedMessage.getBytes().length * 8)>10000) {
          throw new SizeTooBigException("The message size exceeds 10 Kbit limit!");
      } else {

          String msg = "";

          //Do loop to every character in message string
          for (int i = 0; i<encryptedMessage.length(); i++) {
              int res = this.hugKeyString.charAt(i % hugKeyString.length()) ^ seed;
              res-='a';
              res^=encryptedMessage.charAt(i);
              msg += (char) res;

          }

          return msg;

    }
}
}
