import character.*;
import java.util.ArrayList;

public class Game{
    static ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public static Player find(String name){
        for(Player player1 : player){
          if(name.equals(player1.getName())){
            return player1;
          }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        switch (tipe){
          case "Monster":
            if(!isExist(chara)){
              player.add(new Monster(chara,hp));
              return chara + " ditambah ke game";
            }

          case "Magician":
            if(!isExist(chara)){
              player.add(new Magician(chara,hp));
                return chara+" ditambah ke game";

            }

          case "Human":
            if(!isExist(chara)){
              player.add(new Human(chara,hp));
                return chara+" ditambah ke game";

            }

        }
        return "Sudah ada karakter bernama "+ chara;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if(tipe.equals("monster")){
          if(!isExist(chara)){
            player.add(new Monster(chara,hp,roar));
            return chara + " ditambah ke game";
          }
          else{return "Sudah ada karakter bernama "+ chara;}
        }
        else{
          return "Tipe Harus MOnster !!";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if(isExist(chara)){
          for(Player player1 : player){
            if(chara.equals(player1.getName())){
              player.remove(player.indexOf(player1));
              return chara + " dihapus dari game";
            }
          }
        }
        return "Tidak ada " + chara;

    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari g

     ame
     */
    public String status(String chara){
      String result ="";
       if(isExist(chara)){
         result+=find(chara).getType()+" "+ chara +"\n"+"HP: "+find(chara).getHP()+"\n";
         if(find(chara).getHP()==0){
           result+="Sudah meninggal dunia dengan damai\n";
         }
         else{result+="Masih hidup\n";}
         if(find(chara).getDiet().isEmpty()){
           result+="Belum memakan siapa siapa";
         }
         else{
           result+="Memakan "+diet(chara);
         }
       }
       else{
         result+=chara+" tidak ada";
       }
       return result;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String result="";
        for(Player player1 : player){
          result+=status(player1.getName());
          result+="\n";
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String result="";
        for(Player player : find(chara).getDiet()){
          result+= player.getType()+" "+player.getName()+" ";
        }
        return result;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
      String result="";
        for(Player player1 : player ){
          for(Player player2 : player1.getDiet()){
            result+= player2.getType()+" "+player2.getName();
          }
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        if(isExist(meName) && isExist(enemyName)){
          if(find(meName).getHP()!=0){
            if(find(enemyName) instanceof Magician){
              find(enemyName).setHP(find(enemyName).getHP()-20);
              return "nyawa "+enemyName+" "+find(enemyName).getHP();
            }
            else{
              find(enemyName).setHP(find(enemyName).getHP()-10);
              return "nyawa "+enemyName+" "+find(enemyName).getHP();
            }

          }

        }
        return meName +" tidak bisa menyerang";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
      if(isExist(meName) && isExist(enemyName)){
        if(find(meName).getHP()!=0){
          if(find(meName) instanceof Magician){
            if(find(enemyName) instanceof Magician){
              find(enemyName).setHP(find(enemyName).getHP()-20);
            }
            else{
              find(enemyName).setHP(find(enemyName).getHP()-10);
            }
            if(find(enemyName).getHP()==0){
              find(enemyName).setisBurn(true);
              return "Nyawa "+enemyName+" "+find(enemyName).getHP()+" \n dan matang";

            }
            else{
              return "Nyawa "+enemyName+" "+find(enemyName).getHP();
            }
        }
        }
      return meName + " tidak bisa membakar";
    }
    return "Tidak ada "+meName;
  }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        if(isExist(meName) && isExist(enemyName)){
          if(find(meName).getHP()!=0){
            if(find(meName) instanceof Human || find(meName) instanceof Magician){
              if(find(enemyName) instanceof Monster){
                if(find(enemyName).getisBurn()){
                  find(meName).setHP(find(meName).getHP()+15);
                  find(meName).addDiet(find(enemyName));
                  remove(find(enemyName).getName());
                  return meName+ " memakan " +enemyName+"\nNyawa "+meName+" kini "+find(meName).getHP();
                }
              }
            }
            else if (find(meName) instanceof Monster){
              if(find(enemyName).getHP()==0){
                find(meName).setHP(find(meName).getHP()+15);
                find(meName).addDiet(find(enemyName));
                remove(find(enemyName).getName());
                return meName+ " memakan " +enemyName+"\nNyawa "+meName+" kini "+find(meName).getHP();
              }
            }
          }
          return meName+" tidak bisa memakan " +enemyName;
        }
        return "Tidak ada "+meName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
      if(isExist(meName)){
        if(find(meName).getHP()!=0){
          if(find(meName) instanceof Monster ){
            return ((Monster)find(meName)).getRoar();
          }
        }
        return meName + " tidak bisa berteriak";
      }

      return "tidak ada "+meName;

    }

    public static boolean isExist(String chara){
      for(Player player1 : player){
        if(player1.getName().equals(chara)){
          return true;
        }
      }
      return false;
    }
}
