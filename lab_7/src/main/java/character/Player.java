package character;
import java.util.*;

//  write Player Class here
public class Player{
  protected String name, type;
  protected int hp;
  protected ArrayList<Player> diet = new ArrayList<Player>();
  private boolean isburn = false;

  public Player(String name, int hp){
    this.name=name;
    this.hp=hp;
  }

  public void setHP(int hp){
    if(hp<0){
      this.hp=0;
    }
    else{
      this.hp=hp;
    }

  }
  public boolean canEat(Player target){
  if(this instanceof Human || this instanceof Magician){
    if(target instanceof Monster){
      if(target.getisBurn()){
        return true;
      }
    }
  }
  else if(this instanceof Monster){
  if(target.getHP()==0){
      return true;
    }
  }
  return false;
  }

  public void setName(String name){
    this.name = name;
  }
  public int getHp(){
    return hp;
  }
  public int getHP(){
    return hp;
  }
  public String getName(){
    return name;
  }
  public void addDiet(Player diet ){
    this.diet.add(diet);
  }
  public ArrayList<Player> getDiet(){
    return diet;
  }
  public void setisBurn(boolean status){
    isburn = status;
  }
  public boolean getisBurn(){
    return isburn;
  }

  public String getType(){
    return type;
  }
  public String attack(Player target){
	  if(hp!=0){
            if(target instanceof Magician){
              target.setHP(target.getHp()-20);
              return "nyawa "+target.getName()+" "+target.getHp();
            }
            else{
              target.setHP(target.getHp()-10);
              return "nyawa "+target.getName()+" "+target.getHp();
            }
	  }
	  return "Tidak bisa menyerang";
  }
  
}
