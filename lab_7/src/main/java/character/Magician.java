package character;
//  write Magician Class here
public class Magician extends Player{
  public Magician(String name, int hp){
    super(name,hp);
    this.type ="Magician";
  }
  public void burn(Player target){
    if(target instanceof Magician){
      target.setHP(target.getHP()-20);
      if(target.getHP()==0){
        target.setisBurn(true);
      }
    }
    else{
      target.setHP(target.getHP()-10);
      if(target.getHP()==0){
        target.setisBurn(true);
      }
    }

  }

}
