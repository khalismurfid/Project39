import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Khalis Murfid, NPM 1706040164, Kelas DDP-E, GitLab Account: @khalismurfid
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		String warning = "WARNING: Keluarga ini tidak perlu direlokasi!";


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = input.nextInt();
		if (panjang<=0 || panjang>250)
			{ System.out.println(warning);
			return;}
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = input.nextInt();
		if (lebar<=0 || lebar>250)
			{ System.out.println(warning);
			return; }
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = input.nextInt();
		if (tinggi<=0 || tinggi>250)
			{System.out.println(warning);
			return;}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = input.nextFloat();
		if (berat<=0 || berat >150)
			{System.out.println(warning);
			return;}
		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = input.nextInt();
		if (makanan<0 || makanan >20)
			{System.out.println(warning);
			return;}
		input.nextLine();
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = input.nextInt();
		input.nextLine();
		System.out.println();


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		int rasio = (int) (berat*1000000/(panjang*lebar*tinggi));

		for (int a=1; a<=jumlahCetakan; a++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + a + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			System.out.println("DATA SIAP DICETAK UNTUK "+penerima);
			System.out.println("--------------------");
			

			// TODO Periksa ada catatan atau tidak
			if (catatan.length()==0) {catatan="Tidak ada catatan tambahan";}
			else catatan=catatan;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = nama+" - "+alamat+'\n'+"Lahir pada tanggal "
			+tanggalLahir+'\n'+"Rasio Berat Per Volume	"+rasio+ " kg/m^3\n"+catatan;
			System.out.println(hasil);
			System.out.println();
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		short jumlahascii = 0;
		for (int a = 0; a<nama.length();a++)
		{ int x = nama.charAt(a); 
		jumlahascii+=x;}
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int nomor = ((panjang*tinggi*lebar)+jumlahascii)%10000;



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0)+Integer.toString(nomor);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		long anggaran = (int) (50000*365*makanan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]) ; // lihat hint jika bingung
		short umur = (short) (2018-tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String tempat = "";
		String kabupaten = "";
		if (19 <= umur && umur <=1018){
			if (0<=anggaran && anggaran <=100000000){
				tempat = "Teksas";
				kabupaten = "Sastra";
			}
			else {
				tempat = "Mares";
				kabupaten ="Margonda";
			}
		}
		else {
			tempat ="PPMT";
			kabupaten ="Rotunda";
		}
			

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN\n--------------------\nMENGETAHUI: Identitas keluarga: "+
		nama + " - " + nomorKeluarga + "\nMENIMBANG:  Anggaran makanan tahunan: Rp "+ anggaran + 
		"\n            Umur kepala keluarga: " + umur + " tahun\nMEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n"+
		tempat + ", kabupaten " + kabupaten ;
		System.out.println(rekomendasi);

		input.close();
	}
}
