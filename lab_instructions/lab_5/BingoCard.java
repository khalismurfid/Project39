/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
			if (getNumberStates()[num]==null){
				return "Kartu tidak memiliki angka " + num;
			}
			else if(getNumberStates()[num].isChecked()){
				return num+" sebelumnya sudah tersilang";
			}
			else{
				getNumberStates()[num].setChecked(true);
				for (int i = 0; i < 5; i++) {
					if (this.numbers[i][0].isChecked() && this.numbers[i][1].isChecked() &&
						this.numbers[i][2].isChecked() && this.numbers[i][3].isChecked() &&
						this.numbers[i][4].isChecked()) {
						this.isBingo = true;
						break;
					}
					if (this.numbers[0][i].isChecked() && this.numbers[1][i].isChecked() &&
						this.numbers[2][i].isChecked() && this.numbers[3][i].isChecked() &&
						this.numbers[4][i].isChecked()) {
					 	this.isBingo = true;
						break;
					}
				}
					if (this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() &&
						this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() &&
						this.numbers[4][4].isChecked()) {
						this.isBingo = true;
					}
					if (this.numbers[4][0].isChecked() && this.numbers[3][1].isChecked() &&
						this.numbers[2][2].isChecked() && this.numbers[1][3].isChecked() &&
						this.numbers[0][4].isChecked()) {
						this.isBingo = true;
					}
			
				
				return num +" tersilang";
			}
		}	
	public String info(){
		String result = "";
		for(int a =0; a<5; a++){
			for(int b=0;b<5;b++){
				if(getNumbers()[a][b].isChecked()){
					result+= "| X  ";
				}
				else{
					result+="| " + getNumbers()[a][b].getValue()+" ";
				}
			}
			if (a != 4) {
				result += "|\n";
			}
			else{
				result += "|";
			}
		}
		return result;
	}
	
	public void restart(){
		for(int a =0; a<5; a++){
			for(int b=0;b<5;b++){
				getNumbers()[a][b].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
	

}
