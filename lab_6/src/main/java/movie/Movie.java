package movie;

public class Movie{
  private String judul,genre,rating,jenis;
  private int durasi;

  //Constructor
  public Movie(String judul, String rating, int durasi, String genre, String jenis ){
    this.judul = judul;
    this.rating = rating;
    this.durasi = durasi;
    this.genre = genre;
    this.jenis = jenis;
  }

  //Setter Getter
  //----------------------------------------------------------------------------
  public void setJudul(String judul){
    this.judul = judul;
  }
  public void setRating(String rating){
    this.rating = rating;
  }
  public void setDurasi(int durasi){
    this.durasi = durasi;
  }
  public void setGenre(String genre){
    this.genre = genre;
  }
  public void setJenis(String jenis){
    this.judul = jenis;
  }

  public String getJudul(){
    return judul;
  }
  public String getRating(){
    return rating;
  }
  public int getDurasi(){
    return durasi;
  }
  public String getGenre(){
    return genre;
  }
  public String getJenis(){
    return jenis;
  }
  //----------------------------------------------------------------------------

  public String toString(){
    return "------------------------------------------------------------------\nJudul   : "+
            judul+"\nGenre   : "+genre+"\nDurasi  : "+
            durasi+" menit\nRating  : "+rating+"\nJenis   : Film "+jenis+
            "\n------------------------------------------------------------------\n";
  }

  public boolean equals(Object object){
    if(object instanceof Movie){
      Movie that = (Movie) object;
      return this.judul.equals(that.judul) && this.genre.equals(that.genre) && this.jenis.equals(that.jenis) && this.durasi==that.durasi && this.rating.equals(that.rating);
    }
    return false;
  }

}
