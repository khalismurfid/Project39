package customer;
import theater.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Customer{
  private String nama;
  private String kelamin;
  private int umur;
  private ArrayList<Ticket> latest = new ArrayList<Ticket>();

  public Customer(String nama, String kelamin, int umur){
    this.nama = nama;
    this.kelamin = kelamin;
    this.umur = umur;
  }
  //----------------------------------------------------------------------------

  public void setNama(String nama){
    this.nama = nama;
  }
  public void setKelamin(String kelamin){
    this.kelamin = kelamin;
  }
  public void setUmur(int umur){
    this.umur = umur;
  }
  public void setLatest(ArrayList<Ticket> latest){
    this.latest = latest;
  }

  public String getNama(){
    return nama;
  }
  public String getKelamin(){
    return kelamin;
  }
  public int getUmur(){
    return umur;
  }
  public ArrayList<Ticket> getLatest(){
    return latest;
  }
  //----------------------------------------------------------------------------

  public Ticket orderTicket(Theater bioskop, String film, String hari, String jenis){
    for(Ticket ticket : bioskop.getTicket()){
      Movie movie = ticket.getFilm();
      String rating = movie.getRating();
      if(movie.getJudul().equals(film) && ticket.getHari().equals(hari) && ticket.getJenis().equals(jenis)){
        if(rating.equals("Umum")){
          System.out.println(nama+" telah membeli tiket "+film+" jenis "+jenis+" di "+bioskop.getNama()+" pada hari "+hari+" seharga Rp. "+ticket.getHarga());
          bioskop.setSaldo(bioskop.getSaldo()+ticket.getHarga());
          latest.add(ticket);
          return ticket;
        }
        if(rating.equals("Remaja")){
          if(umur>=13){
            System.out.println(nama+" telah membeli tiket "+film+" jenis "+jenis+" di "+bioskop.getNama()+" pada hari "+hari+" seharga Rp. "+ticket.getHarga());
            bioskop.setSaldo(bioskop.getSaldo()+ticket.getHarga());
            latest.add(ticket);
            return ticket;
          }
          else{
            System.out.println(nama+" masih belum cukup umur untuk menonton "+film+" dengan rating "+rating);
            return null;
          }
        }
        if(rating.equals("Dewasa")){
          if(umur>=17){
            System.out.println(nama+" telah membeli tiket "+film+" jenis "+jenis+" di "+bioskop.getNama()+" pada hari "+hari+" seharga Rp. "+ticket.getHarga());
            bioskop.setSaldo(bioskop.getSaldo()+ticket.getHarga());
            latest.add(ticket);
            return ticket;
          }
          else{
            System.out.println(nama+" masih belum cukup umur untuk menonton "+film+" dengan rating "+rating);
            return null;
          }
        }
      }
    }
    System.out.println("Tiket untuk film "+film+" jenis "+jenis+" dengan jadwal "+hari+" tidak tersedia di "+bioskop.getNama());
    return null;
  }

  public void findMovie(Theater bioskop, String film){
    Movie[] movie = bioskop.getFilm();
    int panjang = movie.length;
    for(int a=0; a<panjang;a++){
      if(film.equals(movie[a].getJudul())){
        System.out.print(movie[a]);
        return;
      }
    }
    System.out.println("Film "+film+" yang dicari "+nama+" tidak ada di bioskop "+bioskop.getNama());
    return;
  }

  public void cancelTicket(Theater theater){
    if(!latest.get(latest.size()-1).getStatus()){
      for(Movie movie : theater.getFilm()){
        if (latest.get(latest.size()-1).getFilm().equals(movie)){
          if(theater.getSaldo()-latest.get(latest.size()-1).getHarga()>=0){
            System.out.println("Tiket film "+latest.get(latest.size()-1).getFilm().getJudul()+" dengan waktu tayang "+ latest.get(latest.size()-1).getHari()+" jenis "+latest.get(latest.size()-1).getJenis()+" dikembalikan ke bioskop "+theater.getNama());
            theater.setSaldo(theater.getSaldo()-latest.get(latest.size()-1).getHarga());
            latest.remove(latest.size()-1);
            return;
          }
          else{
            System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop "+theater.getNama()+" lagi tekor...");
            return;
          }
        }
      }
      System.out.println("Maaf tiket tidak bisa dikembalikan, "+latest.get(latest.size()-1).getFilm().getJudul()+" tidak tersedia dalam "+theater.getNama());
    }
    else{
      System.out.println("Tiket tidak bisa dikembalikan karena film "+latest.get(latest.size()-1).getFilm().getJudul()+" sudah ditonton oleh "+nama);
    }
  }

  public void watchMovie(Ticket ticket){
    System.out.println("Kak Pewe telah menonton film "+ticket.getFilm().getJudul());
    ticket.setStatus(true);
  }


}
