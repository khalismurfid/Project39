package ticket;
import movie.*;

public class Ticket{

  private final int hargaTiketBiasa = 60000;
  private final int hargaTiket3D = 72000;
  private final int hargaTiketLibur = 100000;
  private final int hargaTiketLibur3D = 120000;
  private Movie film;
  private String hari;
  private boolean tigadimensi;
  private String jenis;
  private boolean status;

  //Constructor
  public Ticket(Movie film, String hari, boolean tigadimensi){
    this.film = film;
    this.hari = hari;
    this.tigadimensi = tigadimensi;
    if(tigadimensi){
      this.jenis = "3 Dimensi";
    }
    else{
      this.jenis = "Biasa";
    }
  }

  //Setter Getter
  //----------------------------------------------------------------------------
  public void setFilm(Movie film){
    this.film = film;
  }
  public void setHari(String hari){
    this.hari = hari;
  }
  public void setTigaDimensi(boolean tigadimensi){
    this.tigadimensi = tigadimensi;
  }
  public void setStatus(boolean status){
    this.status = status;
  }


  public Movie getFilm(){
    return film;
  }
  public String getHari(){
    return hari;
  }
  public boolean getTigaDimensi(){
    return tigadimensi;
  }
  public int getHarga(){
    if(hari.equals("Sabtu")||hari.equals("Minggu")){
      if(tigadimensi){
        return hargaTiketLibur3D;
      }
      else{
        return hargaTiketLibur;
      }
    }
    else{
      if(tigadimensi){
        return hargaTiket3D;
      }
      else{
        return hargaTiketBiasa;
      }
    }
  }
  public String getJenis(){
    return jenis;
  }
  public boolean getStatus(){
    return status;
  }

  //----------------------------------------------------------------------------

  public String toString(){
    return "------------------------------------------------------------------\nFilm            : "+
            film.getJudul()+"\nJadwal Tayang   : "+hari+"\nJenis           : "+jenis+
            "\n------------------------------------------------------------------";

  }


}
