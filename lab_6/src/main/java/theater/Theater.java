package theater;
import java.util.ArrayList;
import movie.*;
import ticket.*;

public class Theater{

  private String nama;
  private int saldo;
  private ArrayList<Ticket> ticket;
  private Movie[] film;

  public Theater(String nama, int saldo, ArrayList<Ticket> ticket, Movie[] film){
    this.nama = nama;
    this.saldo = saldo;
    this.ticket = ticket;
    this.film = film;
  }

  //Setter Getter
  //----------------------------------------------------------------------------
  public void setNama(String nama){
    this.nama = nama;
  }
  public void setSaldo(int saldo){
    this.saldo = saldo;
  }
  public void setTicket(ArrayList<Ticket> ticket){
    this.ticket = ticket;
  }
  public void setFilm (Movie[] film){
    this.film = film;
  }


  public String getNama(){
    return nama;
  }
  public int getSaldo(){
    return saldo;
  }
  public ArrayList<Ticket> getTicket(){
    return ticket;
  }
  public Movie[] getFilm(){
    return film;
  }
  //----------------------------------------------------------------------------

  public void printInfo(){
    System.out.println("------------------------------------------------------------------");
    System.out.println("Bioskop                 : "+nama);
    System.out.println("Saldo Kas               : "+saldo);
    System.out.println("Jumlah tiket tersedia   : "+ticket.size());
    System.out.print("Daftar Film tersedia    : ");
    for(int a=0; a<film.length;a++){
      if(a==film.length-1){
        System.out.println(film[a].getJudul());
      }
      else{
        System.out.print(film[a].getJudul()+", ");
      }
    }
    System.out.println("------------------------------------------------------------------");
  }

  public static void printTotalRevenueEarned(Theater[] bioskop){
    int totalpendapatan=0;
    for(Theater theater : bioskop){
      totalpendapatan+=theater.getSaldo();
    }
    System.out.println("Total uang yang dimiliki Koh Mas : Rp. "+totalpendapatan);
    System.out.println("------------------------------------------------------------------");
    for(int a =0; a<bioskop.length; a++){
      if(a==bioskop.length-1){
        System.out.println("Bioskop     : "+bioskop[a].getNama());
        System.out.println("Saldo Kas   : Rp. "+bioskop[a].getSaldo());
        System.out.println("");
        System.out.println("------------------------------------------------------------------");
      }
      else{
      System.out.println("Bioskop     : "+bioskop[a].getNama());
      System.out.println("Saldo Kas   : Rp. "+bioskop[a].getSaldo());
      System.out.println("");
      }
    }
  }

}
