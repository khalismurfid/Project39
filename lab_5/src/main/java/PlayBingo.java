import java.util.Scanner;
public class PlayBingo {

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Number [][] temp = new Number[5][5];
		Number [] tempStates = new Number[100];
		
		for(int a = 0; a<5;a++){
			for(int b = 0; b<5;b++){
				int num = scan.nextInt();
				temp[a][b] = new Number(num,a,b);
				tempStates[num] = temp[a][b];
			}
			scan.nextLine();
		}
		BingoCard player1 = new BingoCard(temp,tempStates);
		while(!player1.isBingo()){
			String input = scan.nextLine();
			String [] result = input.toUpperCase().split(" ");
			switch (result[0]){
				case "MARK":
					System.out.println(player1.markNum(Integer.parseInt(result[1])));
					break;
				case "INFO":
					System.out.println(player1.info());
					break;
				case "RESTART":
					player1.restart();
					break;
				default:
					System.out.println("Incorrect command");
			}
			if(player1.isBingo()){
				System.out.println("BINGO!");
				System.out.println(player1.info());
				break;
			}
				
				
			}
		scan.close();
			
		}
	
	}
	