import java.util.Scanner;
import java.io.*;
import corporate.*;

class Lab8 {
    public static void main(String[] args) {
		File text = new File("input.txt");
		try{
			Scanner input = new Scanner(text);
			while(input.hasNextLine()){

        if(input.hasNextInt()){
          Corporate.setThreshold(Integer.parseInt(input.nextLine()));
        }
        else{
          String[] test = input.nextLine().split(" ");
          if(test[0].equals("TAMBAH_KARYAWAN")){
  					Corporate.add(test[2], test[1], Integer.parseInt(test[3]));
  				}
  				else if(test[0].equals("STATUS")){
  					Corporate.status(test[1]);
  				}
  				else if(test[0].equals("GAJIAN")){
  					Corporate.payday();
  				}
  				else if(test[0].equals("TAMBAH_BAWAHAN")){
  					Corporate.addJunior(test[1], test[2]);
  				}
  				else{
  					System.out.println("Perintah salah tolol");
  				}
        }

			}
		}
		catch(FileNotFoundException e){
			System.out.println("File doesn't exist");
		}
    }
}
