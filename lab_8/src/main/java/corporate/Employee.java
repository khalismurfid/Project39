package corporate;
import java.util.*;
public class Employee{
  final protected int MAX_JUNIOR =10;
  protected String name;
  protected int salary;
  protected int gajianCounter = 0;
  protected ArrayList<Employee> junior = new ArrayList<Employee>();

  public Employee(String name, int salary){
    this.name = name;
    this.salary =salary;
  }

  public String getName(){return name;}
  public void setName(String name){this.name=name;}
  public int getSalary(){return salary;}
  public void setSalary(int salary){this.salary=salary;}
  public ArrayList<Employee> getJunior(){return junior;}
  public int getMAX_JUNIOR(){return MAX_JUNIOR;}
  public void addJunior(Employee employee){
    junior.add(employee);
  }

  public String toString(){
    return name+" "+salary;
  }
  public void payday(){
    gajianCounter+=1;
    if(gajianCounter%6==0){
      raisesalary();
    }
  }
  public void raisesalary(){
    int newSalary = salary+ (salary/10);
    System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari "+salary+" menjadi "+ newSalary);
    salary = newSalary;

  }

  public boolean checkAlrJunior(Employee employee){
    for(Employee emp : junior){
      if (employee == emp){
        return true;
      }
    }
    return false;
  }

  public boolean canRecruit(Employee employee){
    if(this instanceof Manager){
      if(employee instanceof Staff || employee instanceof Intern){
        return true;
      }
    }
    else if(this instanceof Staff){
      if(employee instanceof Intern){
        return true;
      }
    }
    return false;
  }

}
