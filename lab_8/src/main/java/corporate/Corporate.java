package corporate;
import java.util.ArrayList;
public class Corporate{
  static private ArrayList<Employee> employee = new ArrayList<Employee>();
  static private int threshold = 18000;

  public static Employee find(String name){
    for(Employee staff : employee ){
      if(name.equals(staff.getName())){
        return staff;
      }
    }
    return null;
  }

  public static void add(String position, String name, int salary){
    if(employee.size()<10000){
      if(find(name)==null){
        switch(position){
          case "MANAGER":
            employee.add(new Manager(name,salary));
            break;
          case "STAFF":
            if(salary>threshold){
              employee.add(new Manager(name,salary));
              System.out.println("Gaji melebihi batas gaji seorang STAFF sehingga karyawan menjadi MANAGER");
              System.out.println(name+" mulai bekerja sebagai MANAGER di PT. TAMPAN");
              return;
            }
            else{
              employee.add(new Staff(name,salary));
            }
            break;
          case "INTERN":
            employee.add(new Intern(name,salary));
            break;
        }
        System.out.println(name+" mulai bekerja sebagai "+position+" di PT. TAMPAN");
      }
      else{
        System.out.println("Karyawan dengan nama "+name+" telah terdaftar");
      }
    }
    else{
      System.out.println("Karyawan kelebihan");
    }

  }

  public static void status(String name){
    if(find(name)==null){
      System.out.println("Karyawan tidak ditemukan");
    }
    else{
      System.out.println(find(name));
    }
  }

  public static void addJunior(String me, String junior){
    if(find(me)==null || find(junior)==null){
      System.out.println("Nama tidak berhasil ditemukan");
    }
    else{
      if(find(me).getJunior().size()<find(me).getMAX_JUNIOR()){
        if(!find(me).checkAlrJunior(find(junior))){
          if(find(me).canRecruit(find(junior))){
            find(me).addJunior(find(junior));
            System.out.println("Karyawan "+junior+" berhasil ditambahkan menjadi menjadi bawahan "+me);
            return;
          }
        }
        else{
          System.out.println("Karyawan "+ junior+" telah menjadi bawahan "+me);
          return;
        }

      }
      System.out.println("Anda tidak layak memiliki bawahan");
    }
  }

  public static void payday(){
    if(!employee.isEmpty()){
      System.out.println("Semua karyawan telah diberikan gaji");
      for(Employee emp : employee){
        emp.payday();
        if(emp instanceof Staff && emp.getSalary()>threshold){
          employee.set(employee.indexOf(emp), new Manager(emp.getName(),emp.getSalary()));
          System.out.println("Selamat, "+emp.getName()+" telah dipromosikan menjadi MANAGER");
        }
      }

    }
    else{System.out.println("Belom ada employee loh");}
  }

  public static void setThreshold(int Threshold){
    threshold = Threshold;
	System.out.println("Batas gaji menjadi "+ threshold);
  }

}
