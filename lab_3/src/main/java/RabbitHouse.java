import java.util.Scanner;

public class RabbitHouse {
	static int jumlahpalindrom =0;
	public static void main(String [] args) {
		Scanner scan = new Scanner (System.in);
		String tipe = scan.next();
		String kata = scan.next();
		scan.close();
		if (tipe.equals("normal")){
			if (kata.length()<=10) {
			System.out.println(hitung(1,kata.length()));
			}
			else {
				System.out.println("Word must be less than 10 Character");
			}
		}
		else if (tipe.equals("palindrom")) {
			hitungpalindrom(kata);
			System.out.println(jumlahpalindrom);
			
		}
		
		else {
			System.out.println("Wrong type");
		}
	}
	
	public static int hitung(int pengkali, int len) {
		if (len ==1) {
			return 1;
		}
		else {
			return pengkali*len+hitung(pengkali*len,len-1);
		}
		
	}
	
	public static boolean cekpalindrom(String kata) {
	String temp ="";
	for (int i=kata.length()-1;i>=0;i--) {
		temp+=kata.charAt(i);
	}
	if (kata.equals(temp)){
		return true;
	}
	else {
		return false;
	}
	
	}
	
	public static void hitungpalindrom(String kata) {
		if(kata.length()==1) {
			return;
		}
		else {
			if(cekpalindrom(kata)==false) {
				jumlahpalindrom+=1;
				// ini loop buat ngilangin karakter 
				// per posisi trus langsung panggil ulang ke fungsinya lagi
				for (int i=0; i<kata.length();i++) {
					String katabaru = kata.substring(0, i)+kata.substring(i+1);
					hitungpalindrom(katabaru);
			}
				}
				else {
					return;
				}			
		}
	}
}
